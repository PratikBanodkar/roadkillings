package com.pratik.appedia.roadkillings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PreviewActivity extends AppCompatActivity implements LocationFetcher.LocationFetcherInterface, View.OnClickListener {

    private LocationFetcher locationFetcher;
    private TextView textView_LatitudeLongitude;
    public ImageView imageView,imageView_Back,imageView_Save, imageView_Retry;
    public EditText editText_Caption;
    public ProgressBar progressBar,progressBar_Save;
    public ConstraintLayout constraintLayout_LayoutToSave, constraintLayout_PreviewRoot;
    private static int REQUEST_CODE_PERMISSIONS = 20;
    private static int REQUEST_CODE_GOOGLE_GPS_DIALOG = 21;
    private static String[] REQUIRED_PERMISSIONS = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION};
    private static String TAG = PreviewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        bindViews();
        setupLocationFetcher();
        loadImageFromStorage(getIntent().getStringExtra("imageFilePath"));
        locationFetcher.getUserLatLong();
    }

    private void loadImageFromStorage(String path) {
        try {
            File f = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
            imageView.setImageBitmap(rotatedBitmap);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void bindViews() {
        imageView = findViewById(R.id.imageView);
        progressBar = findViewById(R.id.progressBar);
        progressBar_Save = findViewById(R.id.progressBar_Save);
        textView_LatitudeLongitude = findViewById(R.id.textView_LatitudeLongitude);
        imageView_Back = findViewById(R.id.imageView_Back);
        imageView_Save = findViewById(R.id.imageView_Save);
        imageView_Retry = findViewById(R.id.imageView_Retry);
        editText_Caption = findViewById(R.id.editText_Caption);
        constraintLayout_LayoutToSave = findViewById(R.id.constraintLayout_LayoutToSave);
        constraintLayout_PreviewRoot = findViewById(R.id.constraintLayout_PreviewRoot);

        imageView_Back.setOnClickListener(this);
        imageView_Save.setOnClickListener(this);
        imageView_Retry.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_Back:
                File file = new File(getIntent().getStringExtra("imageFilePath"));
                boolean deletedFile = file.delete();
                if(deletedFile)
                    Log.d(TAG,"Back pressed so file deleted");
                else
                    Log.d(TAG,"File not deleted");
                finish();
                break;
            case R.id.imageView_Save:
                new ImageSaverTask(getIntent().getStringExtra("imageFilePath"),this).execute();
                break;
            case R.id.imageView_Retry:
                locationFetcher.getUserLatLong();
                textView_LatitudeLongitude.setText("");
                progressBar.setVisibility(VISIBLE);
                imageView_Retry.setVisibility(GONE);
                break;
        }
    }

    //region SETTING UP LOCATION FETCHER
    private void setupLocationFetcher() {
        locationFetcher = new LocationFetcher(PreviewActivity.this);
        locationFetcher.setLocationFetcherInterface(this);
    }

    @Override
    public void onLocationPermissionsNotGiven() {
        ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
    }

    @Override
    public void onUnexpectedErrorOccurred() {
        setCouldNotGetLocationError();
    }

    @Override
    public void onNeedToShowGoogleDialogForPuttingOnGPS(Exception e) {
        try {
            ResolvableApiException rae = (ResolvableApiException) e;
            rae.startResolutionForResult(this, REQUEST_CODE_GOOGLE_GPS_DIALOG);
        } catch (IntentSender.SendIntentException sie) {
            onUnexpectedErrorOccurred();
        }
    }

    @Override
    public void onUserLatLongFetched(double latitude, double longitude) {
        locationFetcher.stopLocationUpdates();
        textView_LatitudeLongitude.setText(new StringBuilder().append(getAddress(latitude, longitude)).append("\n").append(latitude).append(" , ").append(longitude).toString());
        progressBar.setVisibility(GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                locationFetcher.getUserLatLong();
            } else {
                /*Toast.makeText(this, "Location permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();*/
                setCouldNotGetLocationError();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GOOGLE_GPS_DIALOG) {
            if (resultCode == -1) {
                locationFetcher.getUserLatLong();
            } else{
                setCouldNotGetLocationError();
            }
        }
    }

    private void setCouldNotGetLocationError(){
        textView_LatitudeLongitude.setText(getResources().getString(R.string.could_not_get_your_location_please_try_again));
        progressBar.setVisibility(GONE);
        imageView_Retry.setVisibility(VISIBLE);
    }

    private String getAddress(double latitude, double longitude) {
        String address = null;
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS)
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        return true;
    }
    //endregion
}
