package com.pratik.appedia.roadkillings;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ImageSaverTask extends AsyncTask<Object,Void,Boolean>{

    private String imageFilePath;
    private WeakReference<PreviewActivity> previewActivityWeakReference;

    ImageSaverTask(String imageFilePath, PreviewActivity previewActivity) {
        this.imageFilePath = imageFilePath;
        this.previewActivityWeakReference = new WeakReference<>(previewActivity);
    }

    @Override
    protected void onPreExecute() {
        previewActivityWeakReference.get().editText_Caption.clearFocus();
        previewActivityWeakReference.get().editText_Caption.setEnabled(false);
        previewActivityWeakReference.get().imageView_Save.setVisibility(GONE);
        previewActivityWeakReference.get().progressBar_Save.setVisibility(VISIBLE);
        previewActivityWeakReference.get().imageView_Back.setEnabled(false);
        Snackbar savingSnackbar = Snackbar.make(previewActivityWeakReference.get().constraintLayout_PreviewRoot,"Saving your image...",Snackbar.LENGTH_LONG);
        savingSnackbar.show();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Bitmap bitmap = viewToBitmap(previewActivityWeakReference.get().constraintLayout_LayoutToSave);
        if (bitmap != null) {
            return saveBitmapToFile(bitmap);
        }
        return false;
    }

    private Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private boolean saveBitmapToFile(Bitmap bitmap) {
        File pngFile = new File(imageFilePath);
        try {
            FileOutputStream output = new FileOutputStream(pngFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
           return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        previewActivityWeakReference.get().editText_Caption.setEnabled(true);
        previewActivityWeakReference.get().imageView_Save.setVisibility(VISIBLE);
        previewActivityWeakReference.get().progressBar_Save.setVisibility(GONE);
        previewActivityWeakReference.get().imageView_Back.setEnabled(true);
        if(success){
            Snackbar successSnackbar = Snackbar.make(previewActivityWeakReference.get().constraintLayout_PreviewRoot,"Image Saved Successfully !",Snackbar.LENGTH_LONG);
            successSnackbar.show();
        } else{
            Snackbar failureSnackbar = Snackbar.make(previewActivityWeakReference.get().constraintLayout_PreviewRoot,"Image could not be saved !",Snackbar.LENGTH_LONG);
            failureSnackbar.show();
        }
    }
}
